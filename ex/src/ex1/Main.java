package ex1;

import javax.swing.*;
import java.io.*;

public class Main {
    public static void main(String[] args) {

        JFrame frame = new JFrame("ex1");
        var addBooks = new ex1();

        addBooks.getButton1().addActionListener(e -> {

            File myObj = new File("aq.txt");



            FileOutputStream f1 = null;
            try {
                f1 = new FileOutputStream(myObj);
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }

            BufferedWriter booksList = new BufferedWriter(new OutputStreamWriter(f1));
            try {
                booksList.write(String.valueOf(addBooks.getTextField2()));
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }

            try {
                booksList.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }

        });
        frame.setContentPane(new ex1().getPanel1());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

}
