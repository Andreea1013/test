package ex1;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class ex1 {
    private JPanel panel1;
    private JTextField textField1;
    private JTextField textField2;
    private JButton button1;
    private JTextArea textArea1;

    public JTextArea getTextArea1() {
        return textArea1;
    }

    public ex1() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                File myObj = new File(textField1.getText());



                FileOutputStream f1 = null;
                try {
                    f1 = new FileOutputStream(myObj);
                } catch (FileNotFoundException fileNotFoundException) {
                    fileNotFoundException.printStackTrace();
                }

                BufferedWriter booksList = new BufferedWriter(new OutputStreamWriter(f1));
                try {
                    booksList.write(textField2.getText());
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }

                try {
                    booksList.close();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });
    }

    public JPanel getPanel1() {
        return panel1;
    }

    public JTextField getTextField1() {
        return textField1;
    }

    public JTextField getTextField2() {
        return textField2;
    }

    public JButton getButton1() {
        return button1;
    }



    public static void main(String[] args) {
        JFrame frame = new JFrame("ex1");
        frame.setContentPane(new ex1().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


}

